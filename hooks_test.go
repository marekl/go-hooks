package hooks

import (
	"errors"
	"testing"
)

var testFilterAdd = func(data interface{}) (interface{}, error) {
	switch data := data.(type) {
	case int:
		return data + 1, nil
	}
	return nil, errors.New("Invalid type")
}

var testFilterRet = func(data interface{}) (interface{}, error) {
	return 1, nil
}

var actionNum = 0
var testAction = func(data interface{}) error {
	switch data := data.(type) {
	case int:
		actionNum += data
	}
	return nil
}

var testActionSet = func(_ interface{}) error {
	actionNum = 1
	return nil
}

func TestFilter(t *testing.T) {
	AddFilter("test", testFilterAdd, 1)
	AddFilter("test", testFilterAdd, 1)
	AddFilter("test", testFilterAdd, 2)

	if data, _ := ApplyFilter("test", 0); data != 3 {
		t.Error(`hooks.ApplyFilter("test", 0) != 3`, data)
	}

	AddFilter("test", testFilterRet, 0)

	if data, _ := ApplyFilter("test", 0); data != 4 {
		t.Error(`hooks.ApplyFilter("test", 0) != 4`, data)
	}

	AddFilter("test", testFilterRet, 100)

	if data, _ := ApplyFilter("test", 0); data != 1 {
		t.Error(`hooks.ApplyFilter("test", 0) != 1`, data)
	}
}

func TestAllFilter(t *testing.T) {
	AddFilter("testAll", testFilterAdd, 1)
	AddFilter("testAll", testFilterAdd, 1)
	AddFilter("testAll", testFilterAdd, 2)
	AddFilter("all", testFilterAdd, 100)

	if data, _ := ApplyFilter("testAll", 0); data != 4 {
		t.Error(`hooks.ApplyFilter("testAll", 0) != 4`, data)
	}

	AddFilter("all", testFilterRet, 0)

	if data, _ := ApplyFilter("testAll", 0); data != 5 {
		t.Error(`hooks.ApplyFilter("testAll", 0) != 5`, data)
	}

	AddFilter("all", testFilterRet, 200)

	if data, _ := ApplyFilter("testAll", 0); data != 4 {
		t.Error(`hooks.ApplyFilter("testAll", 0) != 4`, data)
	}
}

func TestAction(t *testing.T) {
	actionNum = 0

	AddAction("test", testAction, 1)
	AddAction("test", testAction, 2)
	AddAction("test", testAction, 3)

	DoAction("test", 1)

	if actionNum != 3 {
		t.Error(`actionNum != 3`, actionNum)
	}
}

func TestAllAction(t *testing.T) {
	actionNum = 0

	AddAction("testAll", testAction, 1)
	AddAction("testAll", testAction, 2)
	AddAction("testAll", testAction, 3)
	AddAction("all", testAction, 10)

	DoAction("testAll", 1)

	if actionNum != 4 {
		t.Error(`actionNum != 4`, actionNum)
	}

	AddAction("all", testActionSet, 1)
	DoAction("testAll", 1)

	if actionNum != 5 {
		t.Error(`actionNum != 5`, actionNum)
	}

	AddAction("all", testActionSet, 100)
	DoAction("testAll", 1)

	if actionNum != 4 {
		t.Error(`actionNum != 4`, actionNum)
	}

}
