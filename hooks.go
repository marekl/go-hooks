package hooks

import "sync"

const DefaultPriority = 10

type FilterCallback func(data interface{}) (interface{}, error)
type Filter struct {
	priority int
	callback FilterCallback
}

type ActionCallback func(data interface{}) error
type Action struct {
	priority int
	callback ActionCallback
}

type Hooks struct {
	filters map[string][]Filter
	actions map[string][]Action
}

var once sync.Once
var instance *Hooks

func Instance() *Hooks {
	once.Do(func() {
		instance = &Hooks{
			filters: make(map[string][]Filter),
			actions: make(map[string][]Action),
		}
	})
	return instance
}

// func New() *Hooks {
// 	return &Hooks{
// 		filters: make(map[string][]filter),
// 		actions: make(map[string][]action),
// 	}
// }
