package hooks

import (
	"sort"
)

func AddFilter(tag string, callback FilterCallback, priorities ...int) {
	hooks := Instance()
	priority := DefaultPriority

	if len(priorities) > 0 {
		priority = priorities[0]
	}

	if _, ok := hooks.filters[tag]; !ok {
		hooks.filters[tag] = make([]Filter, 0, 1)
	}

	hooks.filters[tag] = append(hooks.filters[tag], Filter{
		priority: priority,
		callback: callback,
	})
}

func AddAction(tag string, callback ActionCallback, priorities ...int) {
	hooks := Instance()
	priority := DefaultPriority

	if len(priorities) > 0 {
		priority = priorities[0]
	}

	if _, ok := hooks.actions[tag]; !ok {
		hooks.actions[tag] = make([]Action, 0, 1)
	}

	hooks.actions[tag] = append(hooks.actions[tag], Action{
		priority: priority,
		callback: callback,
	})
}

func ApplyFilter(tag string, data interface{}) (interface{}, error) {
	var err error
	hooks := Instance()

	filters, ok := hooks.filters[tag]
	if !ok {
		return nil, nil
	}

	if _, hasAllFilter := hooks.filters["all"]; tag != "all" && hasAllFilter {
		if data, err = ApplyFilter("all", data); err != nil {
			return data, err
		}
	}

	filterMap := make(map[int][]FilterCallback)
	for _, filter := range filters {
		if _, ok := filterMap[filter.priority]; !ok {
			filterMap[filter.priority] = make([]FilterCallback, 0, 1)
		}
		filterMap[filter.priority] = append(filterMap[filter.priority], filter.callback)
	}

	filterPriorities := make([]int, len(filterMap))
	for priority := range filterMap {
		filterPriorities = append(filterPriorities, priority)
	}

	sort.Ints(filterPriorities)

	var e error

	for _, priority := range filterPriorities {
		filters := filterMap[priority]

		for _, filter := range filters {
			data, e = filter(data)
			if e != nil {
				return data, e
			}
		}
	}

	return data, nil
}

func DoAction(tag string, data interface{}) error {
	hooks := Instance()

	filters, ok := hooks.actions[tag]
	if !ok {
		return nil
	}

	if _, hasAllAction := hooks.actions["all"]; tag != "all" && hasAllAction {
		if err := DoAction("all", data); err != nil {
			return err
		}
	}

	actionMap := make(map[int][]ActionCallback)
	for _, filter := range filters {
		if _, ok := actionMap[filter.priority]; !ok {
			actionMap[filter.priority] = make([]ActionCallback, 0, 1)
		}
		actionMap[filter.priority] = append(actionMap[filter.priority], filter.callback)
	}

	actionPriorities := make([]int, len(actionMap))
	for priority := range actionMap {
		actionPriorities = append(actionPriorities, priority)
	}

	sort.Ints(actionPriorities)

	var e error

	for _, priority := range actionPriorities {
		actions := actionMap[priority]

		for _, action := range actions {
			e = action(data)
			if e != nil {
				return e
			}
		}
	}

	return nil
}
